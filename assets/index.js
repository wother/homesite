$$("#themeToggleBtn").addEventListener('click', (evt) => {
    let $$body = $$("body");
    $$body.classList.toggle("dark-mode");
    $$body.classList.toggle("light-mode");
});


// Helper Functions

/**
 * Wrapper for Query selector.
 * @param {String} selector 
 * @returns {HTMLElement}   Selected Element
 */
function $$ (selector) {
    return document.querySelector(selector);
}